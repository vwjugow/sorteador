package ar.com.f11;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

public class Main {

	private static final int JUGADORES = 11;
	private static final String FRANCIA = "Le France";
	private static final String ARGENTINA = "Argentina";
	private static final String SEPARACION = "                    ";

	public static void main(String[] args) throws IOException {
		BufferedReader br = obtenerLector(args);
		String[][] parejas = obtenerParejas(br);
		br.close();
		String[][] equipos = generarEquipos(parejas);
		mostrarEquipos(equipos);
	}

	private static void mostrarEquipos(String[][] equipos) {
		System.out.println(ARGENTINA + SEPARACION + FRANCIA);
		for (int i = 0; i < equipos[0].length; i++) {
			String argentino = equipos[0][i];
			String frances = equipos[1][i];
			System.out.println(argentino + SEPARACION + frances);
		}
	}

	private static String[][] generarEquipos(String[][] parejas) {
		String[][] equipos = new String[2][11];
		Random aleatorio = new Random();

		for (int i = 0; i < equipos[0].length; i++) {
			int equipo = aleatorio.nextInt(2);
			int elOtro = (equipo + 1) % 2;
			equipos[equipo][i] = parejas[i][0];
			equipos[elOtro][i] = parejas[i][1];
		}
		return equipos;
	}

	private static String[][] obtenerParejas(BufferedReader br) {
		String[][] parejas = new String[11][2];
		for (int i = 0; i < JUGADORES; i++) {
			String line = null;
			try {
				line = br.readLine();
			} catch (IOException e) {
				e.printStackTrace();
				error("No se que paso, fijate");
			}
			parejas[i] = line.split(",");
		}
		return parejas;
	}

	private static BufferedReader obtenerLector(String[] args) {
		if (args.length == 0) {
			error("Donde ta el archivo forro?");
		}
		String path = args[0];
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(path));
		} catch (FileNotFoundException e) {
			error("No encuentro el archivo idiota");
		}
		return br;
	}

	private static void error(String s) {
		System.out.println(s);
		System.exit(-1);
	}
}
